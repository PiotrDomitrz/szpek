using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace SzPEK.Models
{
    public class Course
    {
        public int CourseID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        
        [Display(Name = "Starting Date")]
        [DataType(DataType.Date)]
        public DateTime StartingDate { get; set; }
       
        public IList<CourseParticipate> CourseParticipates {get; set;}

    }
}