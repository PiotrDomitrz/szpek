using System;
using System.Collections.Generic;

namespace SzPEK.Models
{
    public class CourseParticipatesViewModel
    {
        public Course Course {get; set;}
        public List<Participate> Participates {get; set;}
    } 
}