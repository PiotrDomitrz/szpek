﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SzPEK.Migrations
{
    public partial class nextMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CourseParticipates_Courses_CourseID",
                table: "CourseParticipates");

            migrationBuilder.DropForeignKey(
                name: "FK_CourseParticipates_Participates_ParticipateID",
                table: "CourseParticipates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CourseParticipates",
                table: "CourseParticipates");

            migrationBuilder.RenameTable(
                name: "CourseParticipates",
                newName: "CourseParticipate");

            migrationBuilder.RenameIndex(
                name: "IX_CourseParticipates_ParticipateID",
                table: "CourseParticipate",
                newName: "IX_CourseParticipate_ParticipateID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CourseParticipate",
                table: "CourseParticipate",
                columns: new[] { "CourseID", "ParticipateID" });

            migrationBuilder.AddForeignKey(
                name: "FK_CourseParticipate_Courses_CourseID",
                table: "CourseParticipate",
                column: "CourseID",
                principalTable: "Courses",
                principalColumn: "CourseID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CourseParticipate_Participates_ParticipateID",
                table: "CourseParticipate",
                column: "ParticipateID",
                principalTable: "Participates",
                principalColumn: "ParticipateID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CourseParticipate_Courses_CourseID",
                table: "CourseParticipate");

            migrationBuilder.DropForeignKey(
                name: "FK_CourseParticipate_Participates_ParticipateID",
                table: "CourseParticipate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CourseParticipate",
                table: "CourseParticipate");

            migrationBuilder.RenameTable(
                name: "CourseParticipate",
                newName: "CourseParticipates");

            migrationBuilder.RenameIndex(
                name: "IX_CourseParticipate_ParticipateID",
                table: "CourseParticipates",
                newName: "IX_CourseParticipates_ParticipateID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CourseParticipates",
                table: "CourseParticipates",
                columns: new[] { "CourseID", "ParticipateID" });

            migrationBuilder.AddForeignKey(
                name: "FK_CourseParticipates_Courses_CourseID",
                table: "CourseParticipates",
                column: "CourseID",
                principalTable: "Courses",
                principalColumn: "CourseID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CourseParticipates_Participates_ParticipateID",
                table: "CourseParticipates",
                column: "ParticipateID",
                principalTable: "Participates",
                principalColumn: "ParticipateID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
