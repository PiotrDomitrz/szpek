using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SzPEK.Models;
using Microsoft.EntityFrameworkCore;

namespace SzPEK.Controllers
{

    public class CoursesController : Controller
    {
        private readonly SzpekContext _context;
                       
        public CoursesController(SzpekContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var model = await GetCoursesAsync();
            return View(model);
        }
        
        public async Task<IActionResult> Details(int? id)
        {
            var model = new CourseParticipatesViewModel();
            model.Course = await GetCourseByIdAsync(id);
        
            model.Participates = await _context.Participates
                .Where(x => x.CourseParticipates
                .Any(y => y.CourseID==id))
                .ToListAsync();
            
            if(isModelNotExist(model))
            {
                return NotFound();
            }
            return View(model);
        }

        public async Task<IActionResult> DeleteCourse(int? id)
        {
            var model = await GetParticipateByIdAsync(id);
            if(isModelNotExist(model))
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpPost, ActionName("DeleteCourse")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteCourseConfirmed(int id)
        { 
            var course = await GetCourseByIdAsync(id);
            RemoveCourseFromDataBase(course);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if(isIdNotExist(id))
            {
                return NotFound();
            }
            
            var model = await GetCourseByIdAsync(id);
            
            if(isModelNotExist(model))
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Course course)
        {
            if (id != course.CourseID)
            {
                return NotFound();
            }

            _context.Update(course);
            await _context.SaveChangesAsync();
            return View(course);
        }

        public async Task<IActionResult> Accept(int? courseId, int? participateId)
        {
            var model = await GetParticipateByIdAsync(participateId);
            if(isModelNotExist(model))
            {
                return NotFound();
            }
            return View(new ParticipateCourseIdViewModel{
                Participate = model,
                CourseID = courseId.Value
            });
        }

        [HttpPost, ActionName("Accept")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AcceptConfirmed(int courseId, Participate participate)
        { 

            var participateModel = await GetParticipateByIdAsync(participate.ParticipateID); 
            participateModel.Status = "Zaakceptowano";
            _context.Update(participateModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new {id = courseId});
        }

        public async Task<IActionResult> Reject(int? courseId, int? participateId)
        {
            var model = await GetParticipateByIdAsync(participateId);
            if(isModelNotExist(model))
            {
                return NotFound();
            }
            return View(new ParticipateCourseIdViewModel{
                Participate = model,
                CourseID = courseId.Value
            });
        }

        [HttpPost, ActionName("Reject")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RejectConfirmed(int courseId, Participate participate)
        { 
            var participateModel = await GetParticipateByIdAsync(participate.ParticipateID);
            participateModel.Status = "Odrzucono";
            _context.Update(participateModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new {id = courseId});
        }

        public async Task<IActionResult> Delete(int? courseId, int? participateId)
        {
            var model = await GetParticipateByIdAsync(participateId);
            if(isModelNotExist(model))
            {
                return NotFound();
            }
            return View(new ParticipateCourseIdViewModel{
                Participate = model,
                CourseID = courseId.Value
            });
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int courseId, Participate participate)
        {
            var participateModel = await GetParticipateByIdAsync(participate.ParticipateID);
            _context.Participates.Remove(participateModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new {id = courseId});
        }

        private async Task<List<Course>> GetCoursesAsync()
        {
           return await _context.Courses.ToListAsync();
        }

        private bool isIdNotExist(int? id)
        {
            if (id == null)
            {
                return true;
            }
            return false;
        }

        private async Task<Participate> GetParticipateByIdAsync(int? id)
        {
            return await _context.Participates.SingleOrDefaultAsync(m => m.ParticipateID == id);
        }

        private void RemoveParticipateFromDataBase(Participate participate)
        {
            _context.Participates.Remove(participate);
        }
        
        private void RemoveCourseFromDataBase(Course course)
        {
            _context.Courses.Remove(course);
        }

        private async Task<Course> GetCourseByIdAsync(int? id)
        {
            return await _context.Courses.SingleOrDefaultAsync(m => m.CourseID == id);
        }

        private bool isModelNotExist(Object model)
        {
            if(model==null)
            {
                return true;
            }
            return false;
        }
    }
}
